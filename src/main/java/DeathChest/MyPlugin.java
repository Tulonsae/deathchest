package DeathChest;

import java.util.ArrayList;
import java.util.List;

import PluginReference.MC_Entity;
import PluginReference.MC_EntityType;
import PluginReference.MC_DamageType;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

import org.tulonsae.deathchest.Inventory;
import org.tulonsae.deathchest.Log;
import org.tulonsae.deathchest.PlayerDeathHandler;

/**
 * Main plugin class for DeathChest for use with Rainbow.
 * TODO - check where player died, void means no items saved
 * TODO - add logging file
 * TODO - add perms
 * TODO - add op/admin override
 * TODO - refactor - add World class and remove world stuff from BlockLocation
 *                   for isWithinBorders, and isEmpty....
 *
 * @author Tulonsae
 */
public class MyPlugin extends PluginBase {

    private static final String VERSION = "0.1";
    private static final String NAME = "DeathChest";
    private static final String DESCRIPTION = "Upon death, put player armor and inventory into a chest.";

    private static MC_Server server;
    private static Log log = new Log(NAME, VERSION);

    public static int maxBuildHeight;
    public static int maxNetherHeight = 128;

    /**
     * Gets the server.
     *
     * @return the server object
     */
    static public MC_Server getServer() {
        return server;
    }

    /**
     * Gets the logger.
     *
     * @return the logging object
     */
    static public Log getLog() {
        return log;
    }

    /**
     * Set this plugin information for Rainbow.
     */
    public PluginInfo getPluginInfo() {
        PluginInfo info = new PluginInfo();
        info.name = "DeathChest";
        info.version = VERSION;
        info.description = "Upon death, put player armor and inventory into a chest.";
        return info;
    }

    /**
     * Startup actions for this plugin.
     *
     * @param server Rainbow server
     */
    @Override
    public void onStartup(MC_Server server) {
        this.server = server;

        log.onStartup();
        maxBuildHeight = server.getMaxBuildHeight();
    }

    /**
     * Shutdown actions for this plugin.
     */
    @Override
    public void onShutdown() {
        log.onShutdown();
    }

    /**
     * Listen for player deaths.
     * If the entity about to die is a player, then save their experience,
     * armor, and inventory into a death chest.
     * TODO - experience
     *
     * @param victim the entity about to die
     * @param killer the killer entity, if any
     * @param dmgType the damage type
     * @param dmgAmount the amount of damage (ignored)
     */
    @Override
    public void onAttemptDeath(MC_Entity victim, MC_Entity killer, MC_DamageType dmgType, float dmgAmount) {
        // if not a player, return immediately
        if (victim.getType() != MC_EntityType.PLAYER) {
            return;
        }

        // invoke the handler and prepare for player death
        MC_Player player = (MC_Player)victim;
        PlayerDeathHandler handler = new PlayerDeathHandler(player, killer, dmgType);
        String msg = handler.prepareForDeath();
        player.sendMessage(msg);
    }
} 

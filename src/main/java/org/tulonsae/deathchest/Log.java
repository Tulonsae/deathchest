package org.tulonsae.deathchest;

import java.text.SimpleDateFormat;
import java.util.Date;

import DeathChest.MyPlugin;

/**
 * Logger utility class for DeathChest.
 *
 * @author Tulonsae
 */
public class Log {

    private String nameToken = "";
    private String nameVersion = "";

    /**
     * Creates a log instance.
     *
     * @param name plugin name
     * @param version plugin version
     */
    public Log(String name, String version) {
        nameToken = "[" + name + "]";
        nameVersion = name + " v" + version;
    }

    // Constructs the timestamp for the log message.
    private String getTimeStamp() {
        return "[" + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()) + "]";
    }

    // Constructs the prefix for the log message.
    private String getPrefix() {
        return getTimeStamp() + " " + nameToken;
    } 

    /**
     * Sends plugin startup message to console.
     */
    public void onStartup() {
        System.out.println(getPrefix() + " " + nameVersion + " enabled.");
    }

    /**
     * Sends plugin shutdown message to console.
     */
    public void onShutdown() {
        System.out.println(getPrefix() + " " + nameVersion + " disabled.");
    }

    /**
     * Sends an informational message to the console.
     *
     * @param msg the message to send
     */
    public void info(String msg) {
        System.out.println(getPrefix() + "[INFO] " + msg);
    }

    /**
     * Sends a warn message to the console.
     *
     * @param msg the message to send
     */
    public void warn(String msg) {
        System.out.println(getPrefix() + "[WARN] " + msg);
    }

    /**
     * Sends an error message to the console.
     *
     * @param msg the message to send
     */
    public void error(String msg) {
        System.out.println(getPrefix() + "[ERROR] " + msg);
    }
}

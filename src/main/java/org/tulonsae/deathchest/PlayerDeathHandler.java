package org.tulonsae.deathchest;

import java.text.SimpleDateFormat;
import java.util.Date;

import PluginReference.MC_Chest;
import PluginReference.MC_DamageType;
import PluginReference.MC_DimensionType;
import PluginReference.MC_Entity;
import PluginReference.MC_Player;

import DeathChest.MyPlugin;

/**
 * Handler for onAttemptDeath event for (Rainbow) DeathChest plugin.
 * TODO - make maxRadius configurable
 * TODO - create better space finding algorithm
 *
 * @author Tulonsae
 */
public class PlayerDeathHandler {

    private static final int maxRadius = 3;

    private Log log;
    private int maxSearchHeight;

    private MC_Player player;
    private String cause;
    private String when;
    private BlockLocation deathLocation;
    private BlockLocation chestLocation;
    private BlockLocation chest2Location = null;
    private Inventory allItems;
    private boolean needDoubleChest = false;

    /**
     * Constructs a handler for an onAttemptDeath event.
     * This assigns the player object, the player's items, the player's
     * location, and determines the when and cause of the upcoming death.
     *
     * @param player the player about to die
     * @param killer the entity killing the player, if any
     * @param dmgType the type of damage that the player will die from
     */
    public PlayerDeathHandler(MC_Player player, MC_Entity killer, MC_DamageType dmgType) {
        log = MyPlugin.getLog();

        if (killer != null) {
            cause = killer.getName();
        } else {
            cause = dmgType.toString();
        }

        when = (new SimpleDateFormat("d-MMM-yy HH:mm")).format(new Date());

        deathLocation = (BlockLocation)player.getLocation();
        if (deathLocation.dimension == MC_DimensionType.NETHER) {
            maxSearchHeight = MyPlugin.maxNetherHeight - 3;
        } else {
            maxSearchHeight = MyPlugin.maxBuildHeight - 2;
        }

        allItems = new Inventory(player);
        if (allItems.getCount() > Inventory.NUM_CHEST_SLOTS) {
            needDoubleChest = true;
        }
    }

    /**
     * Handle actions to prepare for the player's death.
     * Create a death chest and store the inventory.
     * If successful, clear the player's inventory.
     *
     * @return a message to send to the player, null if no message
     */
    public String prepareForDeath() {
        String msg = null;

        if (findSpace()) {
            DeathChest deathChest = new DeathChest(player, cause, when, chestLocation, chest2Location);
            if (deathChest.create()) {
                // store inventory
                MC_Chest chest = chestLocation.getWorld().getChestAt(chestLocation);
                chest.setInventory(allItems.getItemStackList(1));
                if (needDoubleChest) {
                    MC_Chest chest2 = chest2Location.getWorld().getChestAt(chest2Location);
                    chest2.setInventory(allItems.getItemStackList(2));
                }

                // remove items
                if (allItems.hasArmor()) {
                    Inventory.removeArmor(player);
                }
                Inventory.removeInventory(player);

                // log it
                log.info("Created DeathChest for " + player.getName() + " at " + chestLocation.toString());
                msg = "Created a DeathChest for you at " + chestLocation.toString();
            } else {
                log.warn("Unknown error trying to create DeathChest for " + player.getName() + "; items dropped at " + deathLocation.toString());
                msg = "Unknown error trying to create DeathChest; items dropped at " + deathLocation.toString();
            }
        } else {
                log.warn("No space to create DeathChest for " + player.getName() + "; items dropped at " + deathLocation.toString());
                msg = "No space for DeathChest.  Items dropped at " + deathLocation.toString();
        }

        return msg;
    }

    /**
     * Find space for the death chest.
     */
    private boolean findSpace() {
        int xDeath = deathLocation.getX();
        int zDeath = deathLocation.getZ();
        int dimension = deathLocation.dimension;
        chestLocation = deathLocation.copy();

        for (int y = deathLocation.getY(); y < maxSearchHeight; y++) {
            chestLocation.setY(y);
            for (int x = xDeath - maxRadius; x <= xDeath + maxRadius; x++) {
                chestLocation.setX(x);
                for (int z = zDeath - maxRadius; z <= zDeath + maxRadius; z++) {
                    chestLocation.setZ(z);
                    // check primary location
                    if (chestLocation.isValidForChest()) {
                        if (needDoubleChest) {
                            // check secondary location
                            if (chestLocation.isAdjacentNorthValidForChest()) {
                                chest2Location = chestLocation.copyToAdjacentNorth();
                                return true;
                            }
                            if (chestLocation.isAdjacentSouthValidForChest()) {
                                chest2Location = chestLocation.copyToAdjacentSouth();
                                return true;
                            }
                            if (chestLocation.isAdjacentWestValidForChest()) {
                                chest2Location = chestLocation.copyToAdjacentWest();
                                return true;
                            }
                            if (chestLocation.isAdjacentEastValidForChest()) {
                                chest2Location = chestLocation.copyToAdjacentEast();
                                return true;
                            }
                        } else {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}

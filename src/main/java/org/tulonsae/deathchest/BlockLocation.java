package org.tulonsae.deathchest;

import java.io.Serializable;

import PluginReference.MC_BlockType;
import PluginReference.MC_DimensionType;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_World;

import DeathChest.MyPlugin;

/**
 * Represents a block location for Rainbow.
 *
 * @author Tulonsae
 */
public class BlockLocation extends MC_Location implements Serializable {

    private int xBlock;
    private int yBlock;
    private int zBlock;

    private MC_World world;
    private boolean isNether;
    private int maxDimensionHeight;

    /**
     * Constructs a block location from individual coordinates.
     *
     * @param x xBlock coordinate
     * @param y yBlock coordinate
     * @param z zBlock coordinate
     * @param dimension dimension identifier
     */
    public BlockLocation(int x, int y, int z, int dimension) {
        super((double)x, (double)y, (double)z, dimension);
        xBlock = x;
        yBlock = y;
        zBlock = z;

        if (this.dimension == MC_DimensionType.NETHER) {
            isNether = true;
            maxDimensionHeight = MyPlugin.maxNetherHeight;
        } else {
            isNether = false;
            maxDimensionHeight = MyPlugin.maxBuildHeight;
        }

        world = MyPlugin.getServer().getWorld(dimension);
    }

    /**
     * Create a new block location object, at the same location.
     */
    public BlockLocation copy() {
        return new BlockLocation(xBlock, yBlock, zBlock, dimension);
    }

    /**
     * Create a new block location, one block to the north.
     *
     * @return a new block location
     */
    public BlockLocation copyToAdjacentNorth() {
        BlockLocation block = copy();
        block.moveNorth();
        return block;
    }

    /**
     * Create a new block location, one block to the south.
     *
     * @return a new block location
     */
    public BlockLocation copyToAdjacentSouth() {
        BlockLocation block = copy();
        block.moveSouth();
        return block;
    }

    /**
     * Create a new block location, one block to the west.
     *
     * @return a new block location
     */
    public BlockLocation copyToAdjacentWest() {
        BlockLocation block = copy();
        block.moveWest();
        return block;
    }

    /**
     * Create a new block location, one block to the east.
     *
     * @return a new block location
     */
    public BlockLocation copyToAdjacentEast() {
        BlockLocation block = copy();
        block.moveEast();
        return block;
    }

    /**
     * Create a new block location, one block up.
     *
     * @return a new block location
     */
    public BlockLocation copyToAdjacentUp() {
        BlockLocation block = copy();
        block.moveUp();
        return block;
    }

    /**
     * Create a new block location, one block down.
     *
     * @return a new block location
     */
    public BlockLocation copyToAdjacentDown() {
        BlockLocation block = copy();
        block.moveDown();
        return block;
    }

    /**
     * Change this block location one block to the north.
     */
    public void moveNorth() {
        zBlock = getNorth();
    }

    /**
     * Change this block location one block to the south.
     */
    public void moveSouth() {
        zBlock = getSouth();
    }

    /**
     * Change this block location one block to the west.
     */
    public void moveWest() {
        xBlock = getWest();
    }

    /**
     * Change this block location one block to the east.
     */
    public void moveEast() {
        xBlock = getEast();
    }

    /**
     * Change this block location one block up.
     */
    public void moveUp() {
        yBlock = getUp();
    }

    /**
     * Change this block location one block down.
     */
    public void moveDown() {
        yBlock = getDown();
    }

    /**
     * Check if this block location is valid for placing a chest.
     * This means this block location and the one above are empty and the
     * location doesn't have any nearby chests that would prevent chest
     * placement.
     * TODO - check for buildable
     *
     * @return true if location is valid
     */
    public boolean isValidForChest() {
        if (isWithinBorders(yBlock) && isEmptyAndAbove(xBlock, yBlock, zBlock)
                && hasNoChestConflict(xBlock, yBlock, zBlock)) {
            return true;
        }

        return false;
    }

    /**
     * Check if adjacent location north of this block location is valid for
     * placing a chest.
     *
     * @return true if location is valid
     */
    public boolean isAdjacentNorthValidForChest() {
        if (isWithinBorders(yBlock) && isEmptyAndAbove(xBlock, yBlock, getNorth())
                && hasNoChestConflict(xBlock, yBlock, getNorth())) {
            return true;
        }

        return false;
    }

    /**
     * Check if adjacent location south of this block location is valid for
     * placing a chest.
     *
     * @return true if location is valid
     */
    public boolean isAdjacentSouthValidForChest() {
        if (isWithinBorders(yBlock) && isEmptyAndAbove(xBlock, yBlock, getSouth())
                && hasNoChestConflict(xBlock, yBlock, getSouth())) {
            return true;
        }

        return false;
    }

    /**
     * Check if adjacent location west of this block location is valid for
     * placing a chest.
     *
     * @return true if location is valid
     */
    public boolean isAdjacentWestValidForChest() {
        if (isWithinBorders(yBlock) && isEmptyAndAbove(getWest(), yBlock, zBlock)
                && hasNoChestConflict(getWest(), yBlock, zBlock)) {
            return true;
        }

        return false;
    }

    /**
     * Check if adjacent location east of this block location is valid for
     * placing a chest.
     *
     * @return true if location is valid
     */
    public boolean isAdjacentEastValidForChest() {
        if (isWithinBorders(yBlock) && isEmptyAndAbove(getEast(), yBlock, zBlock)
                && hasNoChestConflict(getEast(), yBlock, zBlock)) {
            return true;
        }

        return false;
    }

    /**
     * Check if specified coordinates are within world borders.
     * TODO - check for NESW boundaries for this world
     *
     * @param y the up/down coordinate
     * @return true if coordinates are valid
     */
    public boolean isWithinBorders(int y) {
        if ((y < maxDimensionHeight) && (y >= 0)) {
            return true;
        }

        return false;
    }

    /**
     * Check for air at coordinates and 1 block above.
     *
     * @param x the west/east coordinate
     * @param y the up/down coordinate
     * @param z the north/south coordinate
     * @return true if coordinates are valid
     */
    public boolean isEmptyAndAbove(int x, int y, int z) {
        if ((world.getBlockAt(x, y, z).getId() == MC_BlockType.AIR)
                && (world.getBlockAt(x, y + 1, z).getId() == MC_BlockType.AIR)) {
            return true;
        }

        return false;
    }

    /**
     * Check if coordinates have nearby chests (which would prevent chest
     * placement).
     *
     * @param x the west/east coordinate
     * @param y the up/down coordinate
     * @param z the north/south coordinate
     * @return true if coordinates are valid
     */
    public boolean hasNoChestConflict(int x, int y, int z) {
        if ((world.getBlockAt(x - 1, y, z).getId() != MC_BlockType.CHEST)
                && (world.getBlockAt(x + 1, y, z).getId() != MC_BlockType.CHEST)
                && (world.getBlockAt(x, y, z - 1).getId() != MC_BlockType.CHEST)
                && (world.getBlockAt(x, y, z + 1).getId() != MC_BlockType.CHEST)) {
            return true;
        }

        return false;
    }

    /**
     * Gets the X block coordinate of this block location.
     *
     * @return the X coordinate
     */
    public int getX() {
        return xBlock;
    }

    /**
     * Sets the X block coordinate of this block location.
     *
     * @param x the X coordinate
     */
    public void setX(int x) {
        xBlock = x;
    }

    /**
     * Gets the Y block coordinate of this block location.
     *
     * @return the Y coordinate
     */
    public int getY() {
        return yBlock;
    }

    /**
     * Sets the Y block coordinate of this block location.
     *
     * @param y the Y coordinate
     */
    public void setY(int y) {
        yBlock = y;
    }

    /**
     * Gets the Z block coordinate of this block location.
     *
     * @return the Z coordinate
     */
    public int getZ() {
        return zBlock;
    }

    /**
     * Sets the Z block coordinate of this block location.
     *
     * @param z the Z coordinate
     */
    public void setZ(int z) {
        zBlock = z;
    }

    /**
     * Gets the world object for this block location.
     *
     * @return the world object
     */
    public MC_World getWorld() {
        return world;
    }

    /**
     * Get coordinate north of this block location.
     *
     * @return north coordinate
     */
    private int getNorth() {
        return zBlock - 1;
    }

    /**
     * Get coordinate south of this block location.
     *
     * @return south coordinate
     */
    private int getSouth() {
        return zBlock + 1;
    }

    /**
     * Get coordinate west of this block location.
     *
     * @return west coordinate
     */
    private int getWest() {
        return xBlock - 1;
    }

    /**
     * Get coordinate east of this block location.
     *
     * @return east coordinate
     */
    private int getEast() {
        return xBlock + 1;
    }

    /**
     * Get coordinate above this block location.
     *
     * @return up coordinate
     */
    private int getUp() {
        return yBlock + 1;
    }

    /**
     * Get coordinate below this block location.
     *
     * @return down coordinate
     */
    private int getDown() {
        return yBlock - 1;
    }
}

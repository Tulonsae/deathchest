package org.tulonsae.deathchest;

import java.util.ArrayList;
import java.util.List;

import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

/**
 * Inventory object for DeathChest (Rainbow) plugin.
 * Used to transfer armor, inventory, and experience to and from a death chest.
 *
 * Inventory is stored into 2 stacks (of 27 or less) so inventory can be
 * easily added to each chest separately.
 *
 * TODO - Allow for more than 2 stacks.
 *
 * @author Tulonsae
 */
public class Inventory {

    public static final int NUM_ARMOR_SLOTS = 4;
    public static final int NUM_CHEST_SLOTS = 27;

//    private static List<MC_ItemStack> noArmor = new ArrayList<MC_ItemStack>(NUM_ARMOR_SLOTS);

    private List<MC_ItemStack> stack1;
    private List<MC_ItemStack> stack2;
    private boolean hasArmorFlag = false;
    private int count = 0;

    /**
     * Constructs an inventory object from a player.
     *
     * @param player the player
     */
    public Inventory(MC_Player player) {
        stack1 = new ArrayList<MC_ItemStack>();
        stack2 = new ArrayList<MC_ItemStack>();

        List<MC_ItemStack> armor = player.getArmor();
        for (int i = 0; i < NUM_ARMOR_SLOTS; i++) {
            if ((armor.get(i) != null) && (armor.get(i).getCount() != 0)) {
                stack1.add(armor.get(i));
                count++;
            }
        }

        if (count > 0) {
            hasArmorFlag = true;
        }

        List<MC_ItemStack> allItems = player.getInventory();
        for (MC_ItemStack item : allItems) {
            if (item.getCount() != 0) {
                if (count < NUM_CHEST_SLOTS) {
                    stack1.add(item);
                } else {
                    stack2.add(item);
                }
                count++;
            }
        }
    }

    /**
     * Gets whether the player has armor or not.
     *
     * @return true if the player has any armor
     */
    public boolean hasArmor() {
        return hasArmorFlag;
    }

    /**
     * Deletes the specified player's armor.
     * TODO - can you pass an empty List to MC_Player.setArmor()?
     */
    public static void removeArmor(MC_Player player) {
        player.setArmor(new ArrayList<MC_ItemStack>(NUM_ARMOR_SLOTS));
    }

    /**
     * Deletes the specified player's inventory.
     */
    public static void removeInventory(MC_Player player) {
        player.setInventory(new ArrayList<MC_ItemStack>());
    }

    /**
     * Gets the specified inventory stack list.
     * If 2 is not specified, it returns the first stack.
     *
     * @param id either 1 or 2, only checks for 2
     * @return the inventory stack specified
     */
    public List<MC_ItemStack> getItemStackList(int id) {
        if (id == 2) {
            return stack2;
        }

        return stack1;
    }

    /**
     * Gets the count of items stored in this inventory.
     *
     * @return the count of items
     */
    public int getCount() {
        return count;
    }
}

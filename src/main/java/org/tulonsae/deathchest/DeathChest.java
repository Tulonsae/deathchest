package org.tulonsae.deathchest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Sign;
import PluginReference.MC_World;

import DeathChest.MyPlugin;

/**
 * DeathChest object for (Rainbow) DeathChest plugin.
 *
 * @author Tulonsae
 */
public class DeathChest {

    private UUID uuid;
    private BlockLocation chestLocation;
    private BlockLocation chest2Location;
    private BlockLocation signLocation;

    private String playerName;
    private String cause;
    private String when;
    private int dimension;
    private MC_World world;

    /**
     * Constructs a death chest object.
     * 
     * @param player the player
     * @param cause cause of death
     * @param when date and time of death
     * @param chestLocation block location for primary chest
     * @param chest2Location block location for secondary chest, null if none
     */
    public DeathChest(MC_Player player, String cause, String when, BlockLocation chestLocation, BlockLocation chest2Location) {
        uuid = player.getUUID();
        this.chestLocation = chestLocation;
        this.chest2Location = chest2Location;
        signLocation = chestLocation.copyToAdjacentUp();

        playerName = player.getName();
        this.cause = cause;
        this.when = when;
        dimension = chestLocation.dimension;
        world = chestLocation.getWorld();
    }

    /**
     * Create and place the death chest into the world.
     * TODO - add error processing
     *
     * @return true if death placed successfully
     */
    public boolean create() {

        // place the chest(s)
        placeChest(chestLocation);
        if (chest2Location != null) {
            placeChest(chest2Location);
        }

        // place the sign
        placeSign();

        // TODO - lock the death chest here

        return true;
    }

    /**
     * Place the chest (single or double) into the world.
     * The chest will face south and (TODO) will be locked.
     *
     * @param location the world location to put the chest
     */
    private void placeChest(BlockLocation location) {

        // place the chest, 3 faces south
        world.setBlockAt(new MC_Location((double)location.getX(), (double)location.getY(), (double)location.getZ(), dimension), world.getBlockFromName("CHEST"), 3);

       // lock the chest location
    }
    /**
     * Place the sign for the death chest into the world.
     * The chest must be placed first.  The sign will face south and
     * (TODO) be locked.
     */
    private void placeSign() {

        // place the sign, 0 faces south
        world.setBlockAt(new MC_Location((double)signLocation.getX(), (double)signLocation.getY(), (double)signLocation.getZ(), dimension), world.getBlockFromName("STANDING_SIGN"), 0);

        // set the sign text
        MC_Sign sign = world.getSignAt((MC_Location)signLocation);
        List<String> lines = sign.getLines();
        lines.set(0, playerName);
        lines.set(1, "killed by");
        lines.set(2, cause);
        lines.set(3, when);

        // lock the sign location
    }
}
